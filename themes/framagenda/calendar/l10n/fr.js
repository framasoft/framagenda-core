OC.L10N.register(
	"calendar",
	{
		"Share link": "Partager par lien public",
		"Public access": "Accès public",
		"Email link to person" : "Envoyer le lien par courriel",
		"CalDAV address for clients": "Adresse CalDAV pour les clients",
		"WebDAV address for subscriptions": "Adresse WebDAV pour les abonnements",
		"Iframe to integrate": "Code pour intégrer une iframe",
		"Publish URL": "URL publique",
		"Skip simple event editor": "Passer l'éditeur simplifié d'événements"
},
"nplurals=2; plural=(n > 1);");
