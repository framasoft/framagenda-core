<?php /** @var $l OC_L10N */ ?>
<?php
vendor_script('jsTimezoneDetect/jstz');
script('core', [
	'visitortimezone',
	'lostpassword',
	'login'
]);
?>
<script src="/themes/framagenda/core/nav/nav.js" type="text/javascript"></script>

<div class="row">
	<div class="col-md-8 text-center" id="classic">
		<p><img src="/themes/framagenda/core/img/framagenda.png" alt="" class="ombre screenshot" /></p>
	</div>
	<div id="presentation" class="col-md-4">

	<!--[if IE 8]><style>input[type="checkbox"]{padding:0;}</style><![endif]-->
	<form method="post" name="login">
		<fieldset>
		<?php if (!empty($_['redirect_url'])) {
			print_unescaped('<input type="hidden" name="redirect_url" value="' . \OCP\Util::sanitizeHTML($_['redirect_url']) . '">');
		} ?>
			<?php if (isset($_['apacheauthfailed']) && ($_['apacheauthfailed'])): ?>
				<div class="warning">
					<?php p($l->t('Server side authentication failed!')); ?><br>
					<small><?php p($l->t('Please contact your administrator.')); ?></small>
				</div>
			<?php endif; ?>
			<?php foreach($_['messages'] as $message): ?>
				<div class="warning">
					<?php p($message); ?><br>
				</div>
			<?php endforeach; ?>
			<?php if (isset($_['internalexception']) && ($_['internalexception'])): ?>
				<div class="warning">
					<?php p($l->t('An internal error occurred.')); ?><br>
					<small><?php p($l->t('Please try again or contact your administrator.')); ?></small>
				</div>
			<?php endif; ?>
			<div id="message" class="hidden">
				<img class="float-spinner" alt=""
					src="<?php p(image_path('core', 'loading-dark.gif'));?>">
				<span id="messageText"></span>
				<!-- the following div ensures that the spinner is always inside the #message div -->
				<div style="clear: both;"></div>
			</div>
			<p class="grouptop">
				<input type="text" name="user" id="user"
					placeholder="<?php p($l->t('Username or email')); ?>"
					value="<?php p($_['loginName']); ?>"
					<?php p($_['user_autofocus'] ? 'autofocus' : ''); ?>
					autocomplete="on" autocapitalize="off" autocorrect="off" required>
				<label for="user" class="infield"><?php p($l->t('Username or email')); ?></label>
			</p>

			<p class="groupbottom">
				<input type="password" name="password" id="password" value=""
					placeholder="<?php p($l->t('Password')); ?>"
					<?php p($_['user_autofocus'] ? '' : 'autofocus'); ?>
					autocomplete="on" autocapitalize="off" autocorrect="off" required>
				<label for="password" class="infield"><?php p($l->t('Password')); ?></label>
				<input type="submit" id="submit" class="login primary icon-confirm" title="<?php p($l->t('Log in')); ?>" value="" disabled="disabled"/>
			</p>

			<?php if (!empty($_['invalidpassword']) && !empty($_['canResetPassword'])) { ?>
			<a id="lost-password" class="warning" href="<?php p($_['resetPasswordLink']); ?>">
				<?php p($l->t('Wrong password. Reset it?')); ?>
			</a>
			<?php } else if (!empty($_['invalidpassword'])) { ?>
				<p class="warning">
					<?php p($l->t('Wrong password.')); ?>
				</p>
			<?php } ?>
			<?php if ($_['rememberLoginAllowed'] === true) : ?>
			<div class="remember-login-container">
				<?php if ($_['rememberLoginState'] === 0) { ?>
				<input type="checkbox" name="remember_login" value="1" id="remember_login" class="checkbox checkbox--white">
				<?php } else { ?>
				<input type="checkbox" name="remember_login" value="1" id="remember_login" class="checkbox checkbox--white" checked="checked">
				<?php } ?>
				<label for="remember_login"><?php p($l->t('Stay logged in')); ?></label>
			</div>
			<?php endif; ?>
			<input type="hidden" name="timezone-offset" id="timezone-offset"/>
			<input type="hidden" name="timezone" id="timezone"/>
			<input type="hidden" name="requesttoken" value="<?php p($_['requesttoken']) ?>">
		</fieldset>
	</form>
	<?php if (!empty($_['alt_login'])) { ?>
	<form id="alternative-logins">
		<fieldset>
			<legend><?php p($l->t('Alternative Logins')) ?></legend>
			<ul>
				<?php foreach($_['alt_login'] as $login): ?>
					<li><a class="btn btn-success" href="<?php print_unescaped($login['href']); ?>" ><?php p($login['name']); ?></a></li>
				<?php endforeach; ?>
			</ul>
		</fieldset>
	</form>
	<?php } ?>
	<div class="alert alert-danger">
		<b>Framagenda est en version alpha, la sortie officielle est prévue pour début octobre 2016 (si tout va bien).</b>
		L'application n'est pour l'instant pas ouverte aux tests... Tout contenu créé peut disparaître à n'importe quel moment (ne venez pas vous plaindre :P ).
	</div>
	<h2>Qu’est ce que c’est ?</h2>
		<p><b class="frama">Fram</b><b class="vert">agenda</b> est un service en ligne de gestion et de partage de calendriers.</p>
		<p>En créant un compte, vous pouvez :</p>
		<ul>
			<li><strong>Gérer des agendas</strong> en ligne, mais aussi vos <strong>contacts</strong> et vos <strong>listes de tâches</strong>…</li>
			<li><strong>synchronisés</strong> entre vos ordinateurs, tablettes, mobiles… <a href="#TutoSync" data-toggle="modal" class="small" title="Comment synchroniser ses agendas ?">(comment faire ?)</a></li>
			<li>et que <strong>vous pouvez partager</strong> facilement avec vos contacts.</li>
		</ul>
	</div>

    <!-- modale TutoSync -->
    <div class="modal fade" id="TutoSync" tabindex="-1" role="dialog" aria-labelledby="TutoSyncLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span><span class="sr-only">Fermer</span></button>
                    <h1 id="TutoSyncLabel">Synchroniser ses agendas</h1>
                </div>

                <div class="modal-body clearfix">
                    <p>Pour synchroniser vos agendas sur plusieurs appareils,
                    il faut télécharger un client de synchronisation correspondant à votre environnement compatible CalDAV. Les clients ci-dessous ont été testés avec succès.</p>
                    <br>
                    <div class="tab-content col-xs-12">
                        <h2>Applications sur PC et Mac</h2>
						<ul class="list-group">
							<li class="list-group-item bg-info">Thunderbird (PC, Mac & Linux) <a href="https://framagenda.org/doc/Synchronisation/thunderbird.html">(Tutoriel)</a><span class="pull-right">Conseillé !</span></li>
							<li class="list-group-item">iCal (macOS)</li>
							<li class="list-group-item">Evolution (Linux/Gnome)</li>
							<li class="list-group-item">KOrganizer (Linux/KDE)</li>
						</ul>
						<h2>Applications sur Android</h2>
						<ul class="list-group">
							<li class="list-group-item bg-info">DAVdroid <a href="https://framagenda.org/doc/Synchronisation/android.html">(Tutoriel)</a> <span class="pull-right">Conseillé !</span></li>
							<li class="list-group-item">CalDAV-Sync</li>
						</ul>
						<h2>Applications sur iOS</h2>
						<ul class="list-group">
							<li class="list-group-item bg-info">Paramètres d'iOS <a href="https://framagenda.org/doc/Synchronisation/ios.html">(Tutoriel)</a> <span class="pull-right">Conseillé !</span></li>
						</ul>
                    </div>
                </div>

                <div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Fermer</a></div>
            </div>
        </div>
    </div>
    <!-- /modale TutoSync -->

</div>
<hr role="presentation" />
<div class="row">
    <div class="col-md-4" id="tuto-faq">
        <h2>Documentation</h2>
        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-book"></span></p>
        <p>Pour vous aider dans l'utilisation de <b class="frama">Fram</b><b class="vert">agenda</b> et de la mise en place de la synchronisation sur vos appareils, nous avons rédigé une documentation complète ainsi qu'une <abbr title="Foire aux Questions">FAQ</abbr>.</p>
        <p class="text-center"><a href="https://framagenda.org/doc/" class="btn btn-primary">Lire la documentation »</a></p>
   </div>

    <div class="col-md-4" id="le-logiciel">
        <h2>Le logiciel</h2>
        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-cloud"></span></p>
        <p><b class="frama">Fram</b><b class="vert">agenda</b> repose sur le logiciel libre
<a href="https://owncloud.org">ownCloud</a> et son application Calendar.
        Les données sont hébergées sur les serveurs de <b class="frama">Frama</b><b class="soft">soft</b>,
        <p>ownCloud est sous <a href="https://github.com/owncloud/core/blob/master/COPYING-README">licence <abbr title="Affero General Public License">AGPL</abbr></a>.</p>
        <p>Pour synchroniser vos agendas sur vos ordinateurs, tablettes ou mobiles,
        il est nécessaire d’installer <a href="#TutoSync" data-toggle="modal" >un client CalDAV</a>
        correspondant à votre environnement.</p>

    </div>

    <div class="col-md-4" id="jardin">
        <h2>Cultivez votre jardin</h2>
        <p class="text-center" role="presentation"><span class="glyphicon glyphicon-tree-deciduous"></span></p>
        <p>Pour participer au développement du logiciel, proposer des améliorations
            ou simplement le télécharger, rendez-vous sur <a href="https://github.com/owncloud/">le site de développement</a>.</p>
        <br>
        <p>Si vous souhaitez installer ce logiciel pour votre propre usage et ainsi gagner en autonomie, nous vous aidons sur :</p>
        <p class="text-center"><a href="http://framacloud.org/cultiver-son-jardin/installation-de-owncloud/" class="btn btn-success"><span class="glyphicon glyphicon-tree-deciduous"></span> framacloud.org</a></p>
    </div>
